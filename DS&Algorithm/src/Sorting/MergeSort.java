package Sorting;

public class MergeSort {
	public static void main(String arg[]){
		int a[] = {8,1,5,6,7,2,3,4,9};
		
		mergeSort(a);
		for(int i=0;i<a.length;i++)
		System.out.print(" " + a[i]);
	}
	
	
	public static void mergeSort(int[] a) {
		// TODO Auto-generated method stub
		int n= a.length;
		if(n<2) return;
		int mid = n/2;
		int left[] = new int[mid];
		int right[] = new int[n-mid];
		
		for (int i=0;i<mid;i++)
			left[i] = a[i];
		for(int i=mid;i<n;i++)
			right[i-mid] = a[i];
		
		mergeSort(left);
		mergeSort(right);
		merge(left,right,a);
		
	}


	public static void merge(int L[],int R[], int A[]){
		int nL = L.length;
		int nR = R.length;
		int i=0,j=0,k=0;
		
		while(i<nL && j<nR)
		{
			if(L[i] <= R[j])
			{
				A[k]= L[i];
				i++;
			}
			else
			{
				A[k] = R[j];
				j++;
			}
			k++;
		}
		while(i<nL){
			A[k] = L[i];
			k++;i++;
		}
		while(j<nR){
			A[k] = R[j];
			k++;j++;
		}
	}
}
