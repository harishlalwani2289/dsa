package Sorting;

public class QuickSort {
	public static void main(String arg[]){
		int a[] = {8,1,5,6,7,2,3,4,9};
		
		quickSort(a,0,a.length-1);
		for(int i=0;i<a.length;i++)
		System.out.println(a[i]);
	}

	private static void quickSort(int[] a, int start, int end) {
		if(start>=end) return;
		int pIndex = partition(a,start,end);
		quickSort(a, start,pIndex-1 );
		quickSort(a, pIndex+1, end);
		
	}

	private static int partition(int[] a, int start, int end) {
		int pivot = a[end];
		int pIndex = start;
		
		for(int i=start; i<end;i++){
			 if(a[i]<=pivot){
				 int temp = a[i];
				 a[i]= a[pIndex];
				 a[pIndex] = temp;
				 pIndex++;
			 }
		}
		//Swapping the pivot with pIndex
		int temp1 =a[end];
		a[end] = a[pIndex];
		a[pIndex] = temp1;
		
		return pIndex;
		
		
	}
}
