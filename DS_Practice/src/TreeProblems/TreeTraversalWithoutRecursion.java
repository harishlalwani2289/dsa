package TreeProblems;

import java.util.Stack;

import Tree.Node;

public class TreeTraversalWithoutRecursion {
	
	Node root;
	
	public void inorder(){
		if(root == null)
			return;
		
		Stack<Node> stack = new Stack<Node>();
		Node node = root;
		
		while(node != null){
			stack.push(node);
			node = node.lChild;
		}
		System.out.println();
		while(stack.size() > 0){
			
			node = stack.pop();
			System.out.print(node.iData + " ");
			if(node.rChild != null){
				node = node.rChild;
			
				while(node != null){
					stack.push(node);
					node = node.lChild;
				}
			}
		}
		
		
	}

}
