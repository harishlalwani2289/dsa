package ArrayProblems;

public class ReverseAnArray {
	
	public static void main(String args[]){
		int a[] = {1,2,3,4,5,6,7,8};
		reverseArray(a);
		
		System.out.println("After reversing an Array:  ");
		
		for (int i=0;i<a.length;i++)
		{
			System.out.print(a[i] + " ");
		}
		
	}
	
	public static void reverseArray(int a[]){
		int n = a.length;
		int temp =0;
		for(int i=0,j=n-1;i<=j;i++,j--){
			temp = a[i];
			a[i]=a[j];
			a[j]= temp;
		}
	}

}
