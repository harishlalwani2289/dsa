package Tree;

public class Tree implements Cloneable{

	private Node root;
	
	private Node findNode(int key){
		Node current = root;
		while(current.iData != key){
			if(key > current.iData){
				current = current.rChild;
			}
			else
				current = current.lChild;
			if(current == null){
				return null;
			}
		}
		return current;
	}

	
	private void insertNode(int key){
		
		if(root == null){
			root = new Node(key);
		}
		
		Node current = root;
		int flag = 0;
		
		while(flag != 1){
			
			if(key < current.iData)
			{
				if(current.lChild == null){
					current.lChild = new Node(key);
					flag =1;
				}
				else{
					current = current.lChild;
				}
			}
			if(key > current.iData)
			{
				if(current.rChild == null){
					current.rChild = new Node(key);
					flag =1;
				}
				else{
					current = current.rChild;
				}
			}
		}
		
	}
	
	@Override
	public Object clone() throws CloneNotSupportedException {
		// TODO Auto-generated method stub
		return super.clone();
	}
}
