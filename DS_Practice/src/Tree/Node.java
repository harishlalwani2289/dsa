package Tree;

public class Node {
	
	public int iData;
	public Node rChild;
	public Node lChild;
	
	public Node(int key) {
		// TODO Auto-generated constructor stub
		iData = key;
		rChild = null;
		lChild = null;
	}

}
